# Projet SQL - Training Center (Centre de formation) 

L'objectif du projet sera de faire en groupe la conception du modèle de donnée, la mise en place de la base de données et les composants d'accès aux données pour une application de gestion d'un centre de formation.

Ce projet est réalisé en groupe avec Sérigne, Camille A, Amine et Tarik.
Training-Center est un centre de formation ayant pour but de former des apprenants dans les métiers du numérique.

## Les rôles

Différents rôes et acteurs sont présents.

### Le formateur 
* Rôle du formateur: Le formateur peut assigner des projets aux apprenants et il est liée à une ou plusieurs promotions. Il peut créer des projets et les assigner aux apprenants.

### Le chargé de formation
* Rôle du chargé de formation: Il étudie les différentes candidature des candidats et géré l’admission. Il peut être en charge de plusieurs promotions.

### Le candidat
* Rôle du candidat: Il dépose sa candidature à une formation au sein de l’organisme. Il devient apprenant si celui est admis pour pour une formation.

### L’apprenant
* Rôle de l’apprenant: Un apprenant est assigner à des projets et il appartient à une promotion.

## Les tables

Tous ces rôles constituent des tables.

### Formateur 
* Contient ses informations et son identifiant.

### Chargé de formation 
* Contient ces informations et son identifiant.

### Apprenant 
* Contient ces informations, son identifiant et le nom de sa promotion.

### Candidat 
* Contient ces information, son niveau d’études.

### Formation 
* Contient le nom de la formation avec sa description, sa date de début et sa date de fin.

### Projet 
* Contient le titre du projet avec sa description, ses compétences et une date de début et de fin.

### Promo 
* Contient le nom de la promotion.

## Les Relations

Chacune de ces tables ont des relations et dépendances spécifiques avec d’autre tables.

### Types de relations 

* Le candidat est relié à une seule chargés de formation qui évaluera sa candidature, on parle alors d’une relation one to one.
* En revanche une chargé de formation aura à sa charge plusieurs candidats à traiter, on parle alors d’une relation one to many.
* Et enfin un formateur peut appartenir à plusieurs promotion, tout comme une promotion peut avoir plusieurs formateur, on parle alors cette fois-ci d’une relation many to many. 

## Fonctionnalités du projet

* Un script afin de créer les différentes tables avec leurs propriétés et relations respectives à été crée. Un jeu de données est également fournis dans ce script.
* A été créer une entité et un repository pour chaque tables présentes dans la base données. Les entités contiennent leurs propres propriétés liées aux tables. 
* Les repository contiennent un CRUD complet pour chaque tables
* Une requête spécifique est disponible à la fin du script, Cette requête permet de sélectionner les promos affiliées à un chargé de formation. Une promo ne disposant d'aucun chargé de formation n'est pas sélectionnée.
* Sont mis à disposition un diagramme de use case et un diagramme de classe tous 2 réalisés sous StartUML.

## Le CRUD

La classe DemoCrud contient des appels de méthodes consitutuant le CRUD.

* La méthode findAll. liée au repositoy Trainner : 
Elle permet de sélectionner toutes les propriétés de la table formateur.

* La méthode findById, liée au repository Student :
Elle permet de sélectionner un apprenant en fonction de son Id entré en paramètres.

* La méthode save, liée au repository Candidate :
Elle exécute une requête de type INSERT qui permet d’ajouter une nouvelle entrée das la table candidat. 

* La méthode update, liée au repository Trainner:
Elle permet de modifier les propriétés d’une entrée en fonction de son Id entré en paramètres.

* La méthode deleteById, liée au repository Candidate:
Elle permet de supprimer un projet fonction de son Id entré en paramètres.

Afin de facilité l’access au test de ses méthodes, un id sera mis par défaut et une classe sera instancié en fonction des attentes de la méthode.

## Les contraintes metiers

Des contraintes métiers ont été mise ne place afin d’assurer le bon fonctionnement des tables et de la base de données. Ces contraintes permettent aussi d’optimiser la mémoire utilisé pour la base de données.

* UNSIGNED est utilisé sur les id en AUTO_INCREMENT afin de retirer les nombre négatifs et d’augmenter la valeur positif des id. Ainsi avoir la possibilité d’en stocker plus. 

* NOT NULL est utilisé dans quasiment tous les champs afin de forcer l’utilisateur à renseigner tous les champs disponibles.

* ON DELETE CASCADE est utilisé dans d’avoir la possibilité de supprimé une entrée ou une table ainsi que la relation avec la table liée. Par exemple si une promo est supprimé, l’id_promo contenu dans la tables des apprenants confirmer supprimera les apprenants.

* Le nombre de caractère disponible pour une propriété est aussi geré. Par exemple un numéro de téléphone pourra posséder 14 caractères maximum. Cela laisse la possibilité à ‘utilisateur de mettre un espace entre chaque paires de numéros.

## Liste non exhaustive de futures corrections et améliorations possibles

* Ajouter les relations many to many dans les repositories.
* Faire en sort qu’une adresse mail ne peut pas être ajoutées 2 fois.
* Empêcher le chargé de formation de créer une formation à une date antérieur à celle de la date actuelle.
* Renforcer le nombre de caractère utilisables dans les différentes propriétés.
* Une propriété «Type de formation» est disponible. Le plus judicieux aurait été d’en faire une table.
* La table promo manque de date de début et de date de fin.
* Ajout de requêtes plus spécifiques.

