package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.simplon.promo18.Entity.Student;

public class StudentRepository {

    public List<Student> findAll() {
        List<Student> list = new ArrayList<>();
        try (Connection connection = DbUtil.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM apprenant");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Student student = new Student(
                        rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getString("email"),
                        rs.getString("identifiant"),
                        rs.getString("mot_de_passe"),
                        rs.getString("adresse"),
                        rs.getDate("date_de_naissance").toLocalDate(),
                        rs.getString("telephone"));

                list.add(student);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        return list;
    }

    /**
     * @param student
     */

    public void save(Student apprenant) {
        try {
            Connection connection = DbUtil.connect();

            PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(
                    "INSERT INTO apprenant (nom, prenom, date_de_naissance, email, adresse,telephone) VALUES (?,?,?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, apprenant.getName());
            stmt.setString(2, apprenant.getFirst_name());
            stmt.setDate(3, Date.valueOf(apprenant.getBirthDate()));
            stmt.setString(4, apprenant.getEmail());
            stmt.setString(5, apprenant.getAdress());
            stmt.setString(6, apprenant.getPhoneNumber());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                apprenant.setId(rs.getInt(1));
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Object> findById(int id) {

        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = (PreparedStatement) connection
                    .prepareStatement("SELECT * FROM apprenant  WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Student apprenant = new Student(
                        rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getString("email"),
                        rs.getString("identifiant"),
                        rs.getString("mot_de_passe"),
                        rs.getString("adresse"),
                        rs.getDate("date_de_naissance").toLocalDate(),
                        rs.getString("telephone"));

                return Optional.of(apprenant);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * 
     * @param Date
     * @param apprenat
     * @return
     */
    public boolean update(Student apprenant) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(
                    "UPDATE apprenant SET nom=?, prenom=?, date_de_naissance=?, email=?, adresse=?, telephone=? WHERE id=?");

            stmt.setString(1, apprenant.getName());
            stmt.setString(2, apprenant.getFirst_name());

            stmt.setDate(3, Date.valueOf(apprenant.getBirthDate()));
            stmt.setString(4, apprenant.getEmail());
            stmt.setString(5, apprenant.getAdress());
            stmt.setString(6, apprenant.getPhoneNumber());
            stmt.setInt(7, apprenant.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    /**
     * @param id
     * @return
     */
    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = (PreparedStatement) connection
                    .prepareStatement("DELETE FROM apprenant WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

}
