package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.simplon.promo18.Entity.Project;

public class ProjectRepository {
    public List<Project> findAll() {
        List<Project> list = new ArrayList<>();
        try (Connection connection = DbUtil.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM projet");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Project project = new Project(
                        rs.getInt("id"),
                        rs.getString("titre"),
                        rs.getString("competence"),
                        rs.getDate("date_de_debut").toLocalDate(),
                        rs.getDate("date_de_fin").toLocalDate(),
                        rs.getString("description"));

                list.add(project);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        return list;
    }

    /**
     * 
     * @param project
     */
    public void save(Project project) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO projet (titre, competence, date_de_debut, date_de_fin, description) VALUES (?,?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, project.getTitle());
            stmt.setString(2, project.getSkill());
            stmt.setDate(3, Date.valueOf(project.getStartDate()));
            stmt.setDate(4, Date.valueOf(project.getEndDate()));
            stmt.setString(5, project.getDescription());

            stmt.executeUpdate();

            /**
            
             */
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                project.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Project> findById(int id) {

        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM projet WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Project project = new Project(
                        rs.getInt("id"),
                        rs.getString("titre"),
                        rs.getString("competence"),
                        rs.getDate("date_de_debut").toLocalDate(),
                        rs.getDate("date_de_fin").toLocalDate(),
                        rs.getString("description"));
                return Optional.of(project);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * @param project
     * 
     * @return
     */
    public boolean update(Project project) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE projet SET titre=?, description=?, date_de_debut=?, date_de_fin=?, description=? WHERE id=?");

            stmt.setString(1, project.getTitle());
            stmt.setString(2, project.getSkill());
            stmt.setDate(3, Date.valueOf(project.getStartDate()));
            stmt.setDate(4, Date.valueOf(project.getEndDate()));
            stmt.setString(5, project.getDescription());
            stmt.setInt(6, project.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    /**
     * 
     * @param id
     * @return
     */
    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM projet WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }
}
