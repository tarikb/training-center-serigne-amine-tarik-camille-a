package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.simplon.promo18.Entity.Trainer;

public class TrainerRepository {
    public List<Trainer> findAll() {
        List<Trainer> list = new ArrayList<>();
        try {
            Connection connection = DbUtil.connect();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formateur");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Trainer trainer = new Trainer(
                        rs.getInt("id"),
                        rs.getString("prenom"),
                        rs.getString("nom"),
                        rs.getString("email"),
                        rs.getString("adresse"),
                        rs.getString("identifiant"),
                        rs.getString("mot_de_passe"),
                        rs.getString("telephone"));

                list.add(trainer);
            }
            connection.close();

        } catch (SQLException e) {

            e.printStackTrace();
        }
        System.out.println(list);
        return list;
    }

    public void save(Trainer trainer) {
        try (Connection connection = DbUtil.connect();) {

            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO formateur (prenom, nom, email, adresse, identifiant, mot_de_passe, telephone) VALUES (?,?,?,?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, trainer.getName());
            stmt.setString(2, trainer.getFirst_name());
            stmt.setString(3, trainer.getEmail());
            stmt.setString(4, trainer.getAdress());
            stmt.setString(5, trainer.getLogin());
            stmt.setString(6, trainer.getPassword());
            stmt.setString(7, trainer.getPhoneNumber());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                trainer.setId(rs.getInt(1));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Trainer> findById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formateur WHERE id=?");

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Trainer trainer = new Trainer(
                        rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getString("email"),
                        rs.getString("identifiant"),
                        rs.getString("mot_de_passe"),
                        rs.getString("adresse"),
                        rs.getString("telephone"));

                return Optional.of(trainer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public boolean update(Trainer trainer) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE formateur SET nom=?, prenom=?, email=?,identifiant=?, mot_de_passe=?, adresse=?, telephone=? WHERE id=?");

            stmt.setString(1, trainer.getName());
            stmt.setString(2, trainer.getFirst_name());
            stmt.setString(3, trainer.getEmail());
            stmt.setString(4, trainer.getLogin());
            stmt.setString(5, trainer.getPassword());
            stmt.setString(6, trainer.getAdress());
            stmt.setString(7, trainer.getPhoneNumber());
            stmt.setInt(8, trainer.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM formateur WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}