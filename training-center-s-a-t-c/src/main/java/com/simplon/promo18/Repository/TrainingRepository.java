package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.simplon.promo18.Entity.Training;

public class TrainingRepository {
    public List<Training> findAll() {
        List<Training> list = new ArrayList<>();
        try (Connection connection = DbUtil.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formation");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Training training = new Training(
                        rs.getInt("id"),
                        rs.getString("type_de_formation"),
                        rs.getString("description"),
                        rs.getDate("date_de_debut").toLocalDate(),
                        rs.getDate("date_de_fin").toLocalDate());

                list.add(training);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        return list;
    }

    /**
     * @param training
     */
    public void save(Training training) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO formation (type_de_formation, description, date_de_debut,date_de_fin) VALUES (?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, training.getTrainingType());
            stmt.setString(2, training.getDescription());
            stmt.setDate(3, Date.valueOf(training.getStartDate()));
            stmt.setDate(4, Date.valueOf(training.getEndDate()));

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                training.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Training> findById(int id) {

        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formation WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Training training = new Training(
                        rs.getInt("id"),
                        rs.getString("type_de_formation"),
                        rs.getString("description"),
                        rs.getDate("date_de_debut").toLocalDate(),
                        rs.getDate("date_de_fin").toLocalDate());
                return Optional.of(training);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * @param training
     * 
     * @return
     */
    public boolean update(Training training) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE formation SET type_de_formation=?, description=?, date_de_debut=?, date_de_fin=? WHERE id=?");

            stmt.setString(1, training.getTrainingType());
            stmt.setString(2, training.getDescription());
            stmt.setDate(3, Date.valueOf(training.getStartDate()));
            stmt.setDate(4, Date.valueOf(training.getEndDate()));
            stmt.setInt(5, training.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    /**
     * @param id
     * @return
     */
    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM formation WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }
}
