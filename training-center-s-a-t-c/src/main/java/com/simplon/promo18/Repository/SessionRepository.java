package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.simplon.promo18.Entity.Session;

public class SessionRepository {

  public void save(Session session) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement statement = connection.prepareStatement("INSERT INTO promo (nom) VALUES(?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      statement.setString(1, session.getName());

      statement.executeUpdate();

      ResultSet rs = statement.getGeneratedKeys();
      if (rs.next()) {
        session.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void deleteById(Session session) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM promo WHERE id = ?");

      statement.setInt(1, session.getId());

      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  public void update(Session session) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE promo SET nom=? WHERE id=?");

      statement.setString(1, session.getName());
      statement.setInt(2, session.getId());

      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
