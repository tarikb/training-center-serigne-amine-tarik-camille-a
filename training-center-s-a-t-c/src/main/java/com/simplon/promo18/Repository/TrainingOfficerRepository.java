package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.simplon.promo18.Entity.TrainingOfficer;

public class TrainingOfficerRepository {
  public List<TrainingOfficer> findAll() {
    List<TrainingOfficer> list = new ArrayList<>();

    try (Connection connection = DbUtil.connect()) {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM charge_de_formation");

      ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        TrainingOfficer trainingOfficer = new TrainingOfficer(
            rs.getInt("id"),
            rs.getString("nom"),
            rs.getString("prenom"),
            rs.getString("email"),
            rs.getString("identifiant"),
            rs.getString("mot_de_passe"),
            rs.getString("adresse"),
            rs.getString("telephone"));

        list.add(trainingOfficer);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return list;
  }

  public void save(TrainingOfficer trainingOfficer) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO candidat (nom, prenom, email, identifiant, password, adresse, telephone) VALUES (?,?,?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, trainingOfficer.getName());
      stmt.setString(2, trainingOfficer.getFirst_name());
      stmt.setString(3, trainingOfficer.getEmail());
      stmt.setString(4, trainingOfficer.getLogin());
      stmt.setString(5, trainingOfficer.getPassword());
      stmt.setString(6, trainingOfficer.getAdress());
      stmt.setString(7, trainingOfficer.getPhoneNumber());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        trainingOfficer.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public Optional<TrainingOfficer> findById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM candidat WHERE id=?");

      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        TrainingOfficer trainingOfficer = new TrainingOfficer(
            rs.getInt("id"),
            rs.getString("nom"),
            rs.getString("prenom"),
            rs.getString("email"),
            rs.getString("identifiant"),
            rs.getString("mot_de_passe"),
            rs.getString("adresse"),
            rs.getString("telephone"));

        return Optional.of(trainingOfficer);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return Optional.empty();
  }

  public boolean update(TrainingOfficer trainingOfficer) {
    try (Connection connection = DbUtil.connect()) {
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE formateur SET nom=?, prenom=?, email=?,identifiant=?, mot_de_passe=?, adresse=?, telephone=? WHERE id=?");

        stmt.setString(1, trainingOfficer.getName());
        stmt.setString(2, trainingOfficer.getFirst_name());
        stmt.setString(3, trainingOfficer.getEmail());
        stmt.setString(4, trainingOfficer.getLogin());
        stmt.setString(5, trainingOfficer.getPassword());
        stmt.setString(6, trainingOfficer.getAdress());
        stmt.setString(7, trainingOfficer.getPhoneNumber());
        stmt.setInt(8, trainingOfficer.getId());

        return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
        e.printStackTrace();
    }
    return false;
}

  public void deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM charge_de_formation WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
