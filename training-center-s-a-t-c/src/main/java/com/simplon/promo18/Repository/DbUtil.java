package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {
    public static Connection connect() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/training_center");
    }
}
