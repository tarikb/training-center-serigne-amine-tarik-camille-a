package com.simplon.promo18.Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.simplon.promo18.Entity.Candidate;

public class CandidateRepository {
    public List<Candidate> findAll() {
        List<Candidate> list = new ArrayList<>();
        try (Connection connection = DbUtil.connect()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM candidat");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Candidate candidate = new Candidate(
                        rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getDate("date_de_naissance").toLocalDate(),
                        rs.getString("email"),
                        rs.getString("adresse"),
                        rs.getString("telephone"));

                list.add(candidate);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        return list;
    }

    /**
     * 
     * @param candidate
     */
    public void save(Candidate candidate) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO candidat (nom, prenom, date_de_naissance, email, adresse, telephone) VALUES (?,?,?,?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, candidate.getName());
            stmt.setString(2, candidate.getFirst_name());

            stmt.setDate(3, Date.valueOf(candidate.getBirthDate()));
            stmt.setString(4, candidate.getEmail());
            stmt.setString(5, candidate.getAdress());
            stmt.setString(6, candidate.getPhoneNumber());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                candidate.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Candidate> findById(int id) {

        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM candidat WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Candidate candidate = new Candidate(
                        rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getDate("date_de_naissance").toLocalDate(),
                        rs.getString("email"),
                        rs.getString("adresse"),
                        rs.getString("telephone"));
                return Optional.of(candidate);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * 
     * 
     * @param candidate
     * 
     * @return
     */
    public boolean update(Candidate candidate) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE candidat SET nom=?, prenom=?, date_de_naissance=?, email=?, adresse=?, telephone=? WHERE id=?");

            stmt.setString(1, candidate.getName());
            stmt.setString(2, candidate.getFirst_name());

            stmt.setDate(3, Date.valueOf(candidate.getBirthDate()));
            stmt.setString(4, candidate.getEmail());
            stmt.setString(5, candidate.getAdress());
            stmt.setString(6, candidate.getPhoneNumber());
            stmt.setInt(7, candidate.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    /**
     * 
     * @param id
     * @return
     */
    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM candidat WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

}
