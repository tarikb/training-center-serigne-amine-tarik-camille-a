package com.simplon.promo18;

import java.time.LocalDate;
import java.util.Scanner;

import com.simplon.promo18.Entity.Candidate;
import com.simplon.promo18.Entity.Trainer;
import com.simplon.promo18.Repository.CandidateRepository;
import com.simplon.promo18.Repository.StudentRepository;
import com.simplon.promo18.Repository.TrainerRepository;
import com.simplon.promo18.Repository.TrainingRepository;

public class DemoCrud {

    public void findAllTranners() {
        TrainingRepository repo = new TrainingRepository();
        System.out.println(repo.findAll());

    }

    public void findStudentById(int id) {
        StudentRepository repo = new StudentRepository();
        System.out.println(repo.findById(id));

    }

    public void saveCandidate() {
        CandidateRepository repo = new CandidateRepository();
        Candidate candidate = new Candidate(1, "Geekin", "Serge",
                LocalDate.parse("1987-07-27"), "serge.geekin@gmail.com", "8 rue Claude Levant", "0777293473");
        repo.save(candidate);
    }

    public void updateTrainer(Trainer trainer) {
        TrainerRepository repo = new TrainerRepository();
        trainer = new Trainer(1, "Bouchenafa", "Tarik", "tarik@tarik.com",
                "boubou", "boubou_mdp", "8 rue ",
                "06 50 50 50 50");
        repo.update(trainer);
    }

    public void deleteCandidateById(int id) {
        CandidateRepository repo = new CandidateRepository();

        repo.deleteById(id);
    }

    public void start() {
        menu();
    }

    public void menu() {
        System.out.println("Quelle requete tester ?");
        System.out.println("1. findAllTranners");
        System.out.println("2. findStudentById");
        System.out.println("3. saveCandidate");
        System.out.println("4. updateTrainner");
        System.out.println("5. deleteCandidateById");
        System.out.println("6. Quitter");

        Scanner scanner = new Scanner(System.in);

        switch (scanner.nextInt()) {
            case 1:
                findAllTranners();
                System.out.println("1---Retour");
                if (scanner.nextInt() == 1) {
                    menu();
                }
                break;
            case 2:
                findStudentById(2);
                System.out.println("1---Retour");
                if (scanner.nextInt() == 1) {
                    menu();
                }
                break;
            case 3:
                saveCandidate();
                System.out.println("1---Retour");
                if (scanner.nextInt() == 1) {
                    menu();
                }
                break;
            case 4:
                TrainerRepository repo = new TrainerRepository();
                Trainer trainer = new Trainer(3, "Bouchenafa", "Tarik", "tarik@tarik.com",
                        "boubou", "boubou_mdp", "8 rue ",
                        "06 50 50 50 50");
                repo.update(trainer);
                if (scanner.nextInt() == 1) {
                    menu();
                }
                break;
            case 5:
                deleteCandidateById(1);
                System.out.println("1---Retour");
                if (scanner.nextInt() == 1) {
                    menu();
                }
                break;
            case 6:
                System.exit(0);
                break;
        }

    }
}
