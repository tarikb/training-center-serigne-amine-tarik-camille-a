package com.simplon.promo18.Entity;

import java.time.LocalDate;

public class Student {

    private Integer id;
    private String name;
    private String first_name;
    private String email;
    private String login;
    private String password;
    private String adress;
    private LocalDate birthDate;
    private String phoneNumber;
    private Integer id_promo;

    public Student() {
    }

    public Student(String name, String first_name, String email, String login, String password, String adress,
            LocalDate birthDate, String phoneNumber) {
        this.name = name;
        this.first_name = first_name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

    public Student(Integer id, String name, String first_name, String email, String login, String password,
            String adress, LocalDate birthDate, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.first_name = first_name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getId_promo() {
        return id_promo;
    }

    public void setId_promo(Integer id_promo) {
        this.id_promo = id_promo;
    }

    @Override
    public String toString() {
        return "Student [adress=" + adress + ", birthDate=" + birthDate + ", email=" + email + ", first_name="
                + first_name + ", id=" + id + ", id_promo=" + id_promo + ", login=" + login + ", name=" + name
                + ", password=" + password + ", phoneNumber=" + phoneNumber + "]";
    }

}