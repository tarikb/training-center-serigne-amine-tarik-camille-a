package com.simplon.promo18.Entity;

import java.time.LocalDate;

public class Candidate {
    private int id;
    private String name;
    private String first_name;
    private LocalDate birthDate;
    private String email;
    private String adress;
    private String phoneNumber;

    public Candidate() {
    }

    public Candidate(String name, String first_name, LocalDate date, String email, String adress,
            String phoneNumber) {
        this.name = name;
        this.first_name = first_name;
        this.birthDate = date;
        this.email = email;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
    }

    public Candidate(int id, String name, String first_name, LocalDate birthDate, String email, String adress,
            String phoneNumber) {
        this.id = id;
        this.name = name;
        this.first_name = first_name;
        this.birthDate = birthDate;
        this.email = email;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Candidate [adress=" + adress + ", birthDate=" + birthDate + ", email=" + email + ", first_name="
                + first_name + ", id=" + id + ", name=" + name + ", phoneNumber=" + phoneNumber + "]";
    }
}
