package com.simplon.promo18.Entity;

public class TrainingOfficer {
    private int id;
    private String name;
    private String first_name;
    private String email;
    private String login;
    private String password;
    private String adress;
    private String phoneNumber;

    public TrainingOfficer() {
    }

    public TrainingOfficer(String name, String first_name, String email, String login, String password, String adress,
            String phoneNumber) {
        this.name = name;
        this.first_name = first_name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
    }

    public TrainingOfficer(int id, String name, String first_name, String email, String login, String password,
            String adress, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.first_name = first_name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
