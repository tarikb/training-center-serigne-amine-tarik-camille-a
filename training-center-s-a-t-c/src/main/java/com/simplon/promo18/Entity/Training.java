package com.simplon.promo18.Entity;

import java.time.LocalDate;

public class Training {
    private int id;
    private String trainingType;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;

    public Training() {
    }

    public Training(String trainingType, String description, LocalDate startDate, LocalDate endDate) {
        this.trainingType = trainingType;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Training(int id, String trainingType, String description, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.trainingType = trainingType;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTrainingType() {
        return trainingType;
    }

    public void setTrainingType(String trainingType) {
        this.trainingType = trainingType;
    }

    @Override
    public String toString() {
        return "Training [description=" + description + ", endDate=" + endDate + ", id=" + id + ", startDate="
                + startDate + ", trainingType=" + trainingType + "]";
    }

}
