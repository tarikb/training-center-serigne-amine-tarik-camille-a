package com.simplon.promo18.Entity;

import java.time.LocalDate;

public class Project {
    private int id;
    private String title;
    private String skill;
    private LocalDate startDate;
    private LocalDate endDate;
    private String description;

    public Project() {
    }

    public Project(String title, String skill, LocalDate startDate, LocalDate endDate, String description) {
        this.title = title;
        this.skill = skill;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
    }

    public Project(int id, String title, String skill, LocalDate startDate, LocalDate endDate, String description) {
        this.id = id;
        this.title = title;
        this.skill = skill;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Project [description=" + description + ", endDate=" + endDate + ", id=" + id + ", skill=" + skill
                + ", startDate=" + startDate + ", title=" + title + "]";
    }
}
