CREATE DATABASE IF NOT EXISTS training_center DEFAULT CHARACTER SET = 'utf8mb4';
USE training_center;
DROP TABLE IF EXISTS formateur_projet_joint;
DROP TABLE IF EXISTS formateur_promo_joint;
DROP TABLE IF EXISTS apprenant_projet_joint;
DROP TABLE IF EXISTS apprenant;
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS projet;
DROP TABLE IF EXISTS candidat;
DROP TABLE IF EXISTS charge_de_formation;
DROP TABLE IF EXISTS formateur;
DROP TABLE IF EXISTS formation;
CREATE TABLE formateur (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(45) NOT NULL,
  prenom VARCHAR(45) NOT NULL,
  email VARCHAR(45) NOT NULL,
  identifiant VARCHAR(45) NOT NULL,
  mot_de_passe VARCHAR(45) NOT NULL,
  adresse VARCHAR(64) NOT NULL,
  telephone VARCHAR(45) NOT NULL
);
CREATE TABLE charge_de_formation (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(45) NOT NULL,
  prenom VARCHAR(45) NOT NULL,
  email VARCHAR(45) NOT NULL,
  identifiant VARCHAR(45) NOT NULL,
  mot_de_passe VARCHAR(45) NOT NULL,
  adresse VARCHAR(64) NOT NULL,
  telephone VARCHAR(45) NOT NULL
);
CREATE TABLE promo (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(45) NOT NULL,
  id_charge_de_formation INT UNSIGNED,
  CONSTRAINT FK_promo_charge_de_formation FOREIGN KEY (id_charge_de_formation) REFERENCES charge_de_formation(id) ON DELETE CASCADE
);
CREATE TABLE apprenant (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(45) NOT NULL,
  prenom VARCHAR(45) NOT NULL,
  email VARCHAR(45) NOT NULL,
  identifiant VARCHAR(45) NOT NULL,
  mot_de_passe VARCHAR(45) NOT NULL,
  adresse VARCHAR(64) NOT NULL,
  date_de_naissance DATE NOT NULL,
  Telephone VARCHAR(45) NOT NULL,
  id_promo INT UNSIGNED,
  CONSTRAINT FK_apprenant_promo FOREIGN KEY (id_promo) REFERENCES promo(id) ON DELETE CASCADE
);
CREATE TABLE projet (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  titre VARCHAR(45) NOT NULL,
  competence VARCHAR(45) NOT NULL,
  date_de_debut DATE NOT NULL,
  date_de_fin DATE NOT NULL,
  description VARCHAR(45) NOT NULL,
  id_formateur INT UNSIGNED ,
  CONSTRAINT FK_projet_formateur FOREIGN KEY (id_formateur) REFERENCES formateur(id) ON DELETE CASCADE
);
CREATE TABLE formation (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  type_de_formation VARCHAR(45) NOT NULL,
  description VARCHAR(560) NOT NULL,
  date_de_debut DATE NOT NULL,
  date_de_fin DATE NOT NULL
);
CREATE TABLE candidat (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(45) NOT NULL,
  prenom VARCHAR(45) NOT NULL,
  date_de_naissance DATE NOT NULL,
  email VARCHAR(45) NOT NULL,
  adresse VARCHAR(64) NOT NULL,
  telephone CHAR(14) NOT NULL,
  id_charge_de_formation INT UNSIGNED,
  CONSTRAINT FK_candidat_charge_de_formation FOREIGN KEY (id_charge_de_formation) REFERENCES charge_de_formation(id) ON DELETE CASCADE,
  id_formation INT UNSIGNED,
  CONSTRAINT FK_candidat_formation FOREIGN KEY (id_formation) REFERENCES formation(id) ON DELETE CASCADE
);
CREATE TABLE formateur_promo_joint (
  id_formateur INT UNSIGNED,
  id_promo INT UNSIGNED,
  CONSTRAINT FK_formateur_promo PRIMARY KEY (id_formateur, id_promo),
  CONSTRAINT FK_formateurpromo_formateur FOREIGN KEY (id_formateur) REFERENCES formateur(id),
  CONSTRAINT FK_formateurpromo_promo FOREIGN KEY (id_promo) REFERENCES promo(id)
);
CREATE TABLE apprenant_projet_joint (
  id_apprenant INT UNSIGNED,
  id_projet INT UNSIGNED,
  CONSTRAINT FK_apprenant_projet PRIMARY KEY (id_apprenant, id_projet),
  CONSTRAINT FK_apprenantprojet_apprenant FOREIGN KEY (id_apprenant) REFERENCES apprenant(id),
  CONSTRAINT FK_apprenantprojet_projet FOREIGN KEY (id_projet) REFERENCES projet(id)
);
CREATE TABLE formateur_projet_joint (
  id_formateur INT UNSIGNED,
  id_projet INT UNSIGNED,
  CONSTRAINT FK_formateur_projet PRIMARY KEY (id_formateur, id_projet),
  CONSTRAINT FK_formateurprojet_formateur FOREIGN KEY (id_formateur) REFERENCES formateur(id),
  CONSTRAINT FK_formateurprojet_projet FOREIGN KEY (id_projet) REFERENCES projet(id)
);
INSERT INTO
  formateur (
    nom,
    prenom,
    email,
    identifiant,
    mot_de_passe,
    adresse,
    telephone
  )
VALUES
  (
    'Pichette',
    'Florence',
    'PichetteFlorence@dayrep.com',
    'Uper1985',
    'eVac3xoo2',
    '54, rue du Général Ailleret 97430 LE TAMPON',
    '02.88.54.39.07'
  ),
  (
    'Senneville',
    'Bruce',
    'BruceSenneville@rhyta.com',
    'Anks1988',
    'iHai9aibowu',
    '68, place de Miremont 93420 VILLEPINTE',
    '01.58.78.37.49'
  ),
  (
    'Lacharité',
    'Valérie',
    'ValerieLacharite@teleworm.us',
    'Efored',
    'Quuthai1Ah',
    '73, rue des Coudriers 68200 MULHOUSE',
    '03.51.52.25.30'
  ),
  (
    'Dufresne',
    'Vernon',
    'VernonDufresne@armyspy.com',
    'Thoon2002',
    'AeJeiye8',
    '25, Cours Marechal-Joffre 95170 DEUIL-LA-BARRE',
    '01.20.07.24.67'
  ),
  (
    'Lacombe',
    'Timothée',
    'TimotheeLacombe@dayrep.com',
    'Dialin87',
    'EeXahs1B',
    '32, avenue Jules Ferry 02200 SOISSONS',
    '03.23.12.06.32'
  );
INSERT INTO
  charge_de_formation (
    nom,
    prenom,
    email,
    identifiant,
    mot_de_passe,
    adresse,
    telephone
  )
VALUES
  (
    'Bilodeau',
    'Christian',
    'ChristianBilodeau@rhyta.com',
    'Whomed',
    'uYaXee5mei',
    '22, Quai des Belges 91300 MASSY',
    '01.22.00.45.59'
  ),
  (
    'Meilleur',
    'Aleron',
    'AleronMeilleur@armyspy.com',
    'Spitied',
    'Xoo0joak',
    '85, rue Grande Fusterie 91800 BRUNOY',
    '01.62.04.09.98'
  ),
  (
    'Binet',
    'Slainie',
    'SlainieBinet@jourrapide.com',
    'Ficese',
    'jeexeic0AeX',
    '54, avenue de l''Amandier92270 BOIS-COLOMBES',
    '01.48.49.49.69'
  ),
  (
    'Chalut',
    'Caresse',
    'CaresseChalut@armyspy.com',
    'Evelostrues',
    'eipooPihee5',
    '89, Cours Marechal-Joffre 59220 DENAIN',
    '03.49.74.09.10'
  ),
  (
    'Dagenais',
    'Viollette',
    'ViolletteDagenais@dayrep.com',
    'Majess',
    'kahchu8Eeyoo',
    '97, place Maurice-Charretier 28000 CHARTRES',
    '02.43.79.84.60'
  );
INSERT INTO
  promo (nom, id_charge_de_formation)
VALUES
  ('Développeur Web', 1),
  ('Développeur Java', 2),
  ('Développeur PHP', 3),
  ('Conecpteur d''application', 4),
  ('Développeur IA', 5);
INSERT INTO
  apprenant (
    nom,
    prenom,
    email,
    identifiant,
    mot_de_passe,
    adresse,
    date_de_naissance,
    telephone,
    id_promo
  )
VALUES
  (
    'Ruel',
    'Melusina',
    'MelusinaRuel@jourrapide.com',
    'Upok1970',
    'neW9tee4sah',
    '39, rue Pierre De Coubertin 31100 TOULOUSE',
    '1970-07-23',
    '05.00.89.17.99',
    1
  ),
  (
    'Lépicier',
    'Émilie',
    'EmilieLepicier@teleworm.us',
    'Theoper',
    'Ei0peech',
    '43, Place de la Gare 68000 COLMAR',
    '1970-10-25',
    '03.94.45.05.33',
    2
  ),
  (
    'Laforge',
    'Leone',
    'LeoneLaforge@teleworm.us',
    'Stentake',
    'aenie7Vohch',
    '89, rue Adolphe Wurtz 76140 LE PETIT-QUEVILLY',
    '1970-04-29',
    '02.60.18.64.72',
    1
  ),
  (
    'Ruel',
    'Melusina',
    'MelusinaRuel@jourrapide.com',
    'Upok1970',
    'neW9tee4sah',
    '39, rue Pierre De Coubertin 31100 TOULOUSE',
    '1970-07-23',
    '05.00.89.17.99',
    3
  ),
  (
    'De La Vergne',
    'Vincent',
    'VincentDeLaVergne@rhyta.com',
    'Walcon',
    'EeCeip9xoo',
    '51, Rue de Strasbourg 92110 CLICHY',
    '1976-01-10',
    '01.56.38.97.55',
    4
  );
INSERT INTO
  projet (
    titre,
    competence,
    description,
    date_de_debut,
    date_de_fin
  )
VALUES
  (
    'projet HTML',
    'HTML',
    'Ceci est un projet HTML',
    '2022-01-01',
    '2022-01-15'
  ),
  (
    'projet CSS',
    'CSS',
    'Ceci est un projet CSS',
    '2022-01-16',
    '2022-02-01'
  ),
  (
    'projet GIT',
    'GIT',
    'Ceci est un projet GIT',
    '2022-02-01',
    '2022-02-15'
  ),
  (
    'projet Java',
    'Java',
    'Ceci est un projet Java',
    '2022-02-16',
    '2022-03-01'
  ),
  (
    'projet SQL',
    'SQL',
    'Ceci est un projet SQL',
    '2022-03-01',
    '2022-03-15'
  );
INSERT INTO
  formation (
    type_de_formation,
    description,
    date_de_debut,
    date_de_fin
  )
VALUES
  (
    'Développeur Web',
    'une formation de développement Web',
    '2022-01-01',
    '2022-06-01'
  ),
  (
    'Développeur Java',
    'une formation de développement Java',
    '2022-02-04',
    '2022-11-14'
  ),
  (
    'Développeur PHP',
    'une formation de développement PHP',
    '2022-06-16',
    '2022-12-04'
  ),
  (
    'Conecpteur d''application',
    'une formation de conecpteur d''application',
    '2022-12-09',
    '2023-06-13'
  ),
  (
    'Développeur IA',
    'une formation de développement IA',
    '2022-04-24',
    '2023-005-26'
  );
INSERT INTO
  candidat (
    nom,
    prenom,
    date_de_naissance,
    email,
    adresse,
    telephone
  )
VALUES
  (
    'Cantin',
    'Normand',
    '1998-04-30',
    'NormandCantin@armyspy.com',
    '9, rue Ernest Renan 50100 CHERBOURG',
    '02.04.52.98.13'
  ),
  (
    'Simon',
    'Dominique',
    '1999-11-19',
    'DominiqueSimon@teleworm.us',
    '71, rue du Château 97480 SAINT-JOSEPH',
    '02.13.18.19.30'
  ),
  (
    'Létourneau',
    'Oliver',
    '1962-06-21',
    'OliverLetourneau@teleworm.us',
    '64, Avenue De Marlioz 13200 ARLES',
    '01.26.09.12.79'
  ),
  (
    'Petit',
    'Blanchefle',
    '1971-08-18',
    'BlancheflePetit@teleworm.us',
    '24, Rue Joseph Vernet 97122 BAIE-MAHAULT',
    '05.89.86.17.08'
  ),
  (
    'Cyr',
    'Mavise',
    '1986-10-20',
    'MaviseCyr@armyspy.comI',
    '30, avenue de l''Amandier41000 BLOIS',
    '02.37.33.96.98'
  );
INSERT INTO
  formateur_promo_joint (id_formateur, id_promo)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (4, 5),
  (3, 4);
INSERT INTO
  apprenant_projet_joint (id_apprenant, id_projet)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (4, 5),
  (3, 4);
INSERT INTO
  formateur_projet_joint (id_formateur, id_projet)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (4, 5),
  (3, 4);
SELECT
  *
FROM
  apprenant
  LEFT JOIN promo ON apprenant.id_promo = promo.id;